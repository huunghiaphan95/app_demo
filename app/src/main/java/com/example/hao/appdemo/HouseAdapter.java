package com.example.hao.appdemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.ViewHolder> {

    ArrayList<House> list;
    Context context;

    public HouseAdapter(ArrayList<House> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_house, viewGroup, false);
        ViewHolder vholder = new ViewHolder(view);
        return vholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        viewHolder.housename.setText(list.get(position).getHousename());
        viewHolder.address.setText(list.get(position).getAddress());

//        viewHolder.housename2.setText(list.get(position).getHousename());
//        viewHolder.address2.setText(list.get(position).getAddress());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView housename,housename2,address,address2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            housename = itemView.findViewById(R.id.tv_housename);
            address = itemView.findViewById(R.id.tv_address);
            housename = itemView.findViewById(R.id.tv_housename2);
            address = itemView.findViewById(R.id.tv_address2);
        }
    }
}
