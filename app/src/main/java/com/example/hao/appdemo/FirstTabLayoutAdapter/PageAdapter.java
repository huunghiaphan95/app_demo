package com.example.hao.appdemo.FirstTabLayoutAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hao.appdemo.FirstTabLayout.Tab_Du_An;
import com.example.hao.appdemo.FirstTabLayout.Tab_Tin_Dang;

public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    public PageAdapter(FragmentManager childFragmentManager) {
        super(childFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Tab_Du_An();
            case 1:
                return new Tab_Tin_Dang();
        }
        return null;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
