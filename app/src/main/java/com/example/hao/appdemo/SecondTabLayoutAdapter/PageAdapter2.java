package com.example.hao.appdemo.SecondTabLayoutAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hao.appdemo.SecondTabLayout.Tab_Dang_Du_An;
import com.example.hao.appdemo.SecondTabLayout.Tab_Du_An_Moi;
import com.example.hao.appdemo.SecondTabLayout.Tab_Loc_Du_An;

public class PageAdapter2 extends FragmentPagerAdapter {

    private int numOfTabs;

    public PageAdapter2(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    public PageAdapter2(FragmentManager childFragmentManager) {
        super(childFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Tab_Du_An_Moi();
            case 1:
                return new Tab_Loc_Du_An();
            case 2:
                return new Tab_Dang_Du_An();
        }
        return null;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
