package com.example.hao.appdemo;

public class House {
    private String housename;
    private String address;

    public House() {
    }

    public House(String housename, String address) {
        this.housename = housename;
        this.address = address;
    }

    public String getHousename() {
        return housename;
    }

    public void setHousename(String housename) {
        this.housename = housename;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "House{" +
                "housename='" + housename + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
