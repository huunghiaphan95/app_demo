package com.example.hao.appdemo.SecondTabLayout;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hao.appdemo.House;
import com.example.hao.appdemo.HouseAdapter;
import com.example.hao.appdemo.R;

import java.util.ArrayList;

public class Tab_Du_An_Moi extends Fragment {
    ArrayList<House> arrHouse;
    HouseAdapter houseAdapter;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_du_an_moi, container, false);

        recyclerView = view.findViewById(R.id.recycleView);

        HouseAdapter houseAdapter = new HouseAdapter(arrHouse, getContext());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.setAdapter(houseAdapter);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrHouse = new ArrayList<>();
        arrHouse.add(new House("Khu biet thu cao cap Cocoland","Duong NE4, Thoi Hoa, Binh Duong"));
        arrHouse.add(new House("Khu biet thu cao cap Cocoland","Duong NE4, Thoi Hoa, Binh Duong"));

    }
}
